#!/bin/bash

parameters="${1}${2}${3}${4}${5}${6}${7}${8}${9}"

Escape_Variables()
{
	text_progress="\033[38;5;113m"
	text_success="\033[38;5;113m"
	text_warning="\033[38;5;221m"
	text_error="\033[38;5;203m"
	text_message="\033[38;5;75m"

	text_bold="\033[1m"
	text_faint="\033[2m"
	text_italic="\033[3m"
	text_underline="\033[4m"

	erase_style="\033[0m"
	erase_line="\033[0K"

	move_up="\033[1A"
	move_down="\033[1B"
	move_foward="\033[1C"
	move_backward="\033[1D"
}

Parameter_Variables()
{
	if [[ $parameters == *"-v"* || $parameters == *"-verbose"* ]]; then
		verbose="1"
		set -x
	fi
}

Path_Variables()
{
	script_path="${0}"
	directory_path="${0%/*}"

	resources_path="$directory_path/patch"

	if [[ -d "/patch" ]]; then
		resources_path="/patch"
	fi
	
	if [[ -d "/Volumes/Image Volume/patch" ]]; then
		resources_path="/Volumes/Image Volume/patch"
	fi
}

Input_Off()
{
	stty -echo
}

Input_On()
{
	stty echo
}

Output_Off()
{
	if [[ $verbose == "1" ]]; then
		"$@"
	else
		"$@" &>/dev/null
	fi
}

Check_Environment()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Checking system environment."${erase_style}

	if [ -d /Install\ *.app ]; then
		environment="installer"
	fi

	if [ ! -d /Install\ *.app ]; then
		environment="system"
	fi

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Checked system environment."${erase_style}
}

Check_Root()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Checking for root permissions."${erase_style}

	if [[ $environment == "installer" ]]; then
		root_check="passed"
		echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Root permissions check passed."${erase_style}
	else

		if [[ $(whoami) == "root" && $environment == "system" ]]; then
			root_check="passed"
			echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Root permissions check passed."${erase_style}
		fi

		if [[ ! $(whoami) == "root" && $environment == "system" ]]; then
			root_check="failed"
			echo -e $(date "+%b %d %H:%M:%S") ${text_error}"- Root permissions check failed."${erase_style}
			echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Run this tool with root permissions."${erase_style}
			Input_On
			exit
		fi

	fi
}

Check_Resources()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Checking for resources."${erase_style}

	if [[ -d "$resources_path" ]]; then
		resources_check="passed"
		echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Resources check passed."${erase_style}
	fi

	if [[ ! -d "$resources_path" ]]; then
		resources_check="failed"
		echo -e $(date "+%b %d %H:%M:%S") ${text_error}"- Resources check failed."${erase_style}
		echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Run this tool with the required resources."${erase_style}

		Input_On
		exit
	fi
}

Input_Model()
{
model_list="/     iMac4,1
/     iMac4,2
/     iMac5,1
/     iMac5,2
/     iMac6,1
/     MacBook2,1
/     MacBook3,1
/     MacBook4,1
/     MacBookAir1,1
/     MacBookPro2,1
/     MacBookPro2,2
/     Macmini1,1
/     Macmini2,1
/     MacPro1,1
/     MacPro2,1
/     Xserve1,1
/     Xserve2,1"

model_ati="iMac4,1
iMac5,1
MacBookPro2,1
MacBookPro2,2
MacPro1,1
MacPro2,1
Xserve1,1
Xserve2,1"

	model_detected="$(sysctl -n hw.model)"

	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Detecting model."${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Detected model as $model_detected."${erase_style}

	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ What model would you like to use?"${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Input an model option."${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/     1 - Use detected model"${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/     2 - Use manually selected model"${erase_style}

	Input_On
	read -e -p "$(date "+%b %d %H:%M:%S") / " model_option
	Input_Off

	if [[ $model_option == "1" ]]; then
		model="$model_detected"
		echo -e $(date "+%b %d %H:%M:%S") ${text_success}"+ Using $model_detected as model."${erase_style}
	fi

	if [[ $model_option == "2" ]]; then
		echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ What model would you like to use?"${erase_style}
		echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Input your model."${erase_style}
		echo -e $(date "+%b %d %H:%M:%S") ${text_message}"$model_list"${erase_style}

		Input_On
		read -e -p "$(date "+%b %d %H:%M:%S") / " model_selected
		Input_Off

		model="$model_selected"
		echo -e $(date "+%b %d %H:%M:%S") ${text_success}"+ Using $model_selected as model."${erase_style}
	fi
}

Input_Volume()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ What volume would you like to use?"${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Input a volume number."${erase_style}

	for volume_path in /Volumes/*; do
		volume_name="${volume_path#/Volumes/}"

		if [[ ! "$volume_name" == com.apple* ]]; then
			volume_number=$(($volume_number + 1))
			declare volume_$volume_number="$volume_name"

			echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/     ${volume_number} - ${volume_name}"${erase_style} | sort
		fi

	done

	Input_On
	read -e -p "$(date "+%b %d %H:%M:%S") / " volume_number
	Input_Off

	volume="volume_$volume_number"
	volume_name="${!volume}"
	volume_path="/Volumes/$volume_name"
}

Check_Volume_Version()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Checking system version."${erase_style}
		
		volume_version="$(defaults read "$volume_path"/System/Library/CoreServices/SystemVersion.plist ProductVersion)"
		volume_version_short="$(defaults read "$volume_path"/System/Library/CoreServices/SystemVersion.plist ProductVersion | cut -c-5)"
		
		volume_build="$(defaults read "$volume_path"/System/Library/CoreServices/SystemVersion.plist ProductBuildVersion)"
	
		if [[ ${#volume_version} == "6" ]]; then
			volume_version_short="$(defaults read "$volume_path"/System/Library/CoreServices/SystemVersion.plist ProductVersion | cut -c-4)"
		fi
	
		if [[ $environment == "installer" ]]; then
			system_volume_version="$(defaults read /System/Library/CoreServices/SystemVersion.plist ProductVersion)"
			system_volume_version_short="$(defaults read /System/Library/CoreServices/SystemVersion.plist ProductVersion | cut -c-5)"
		
			system_volume_build="$(defaults read /System/Library/CoreServices/SystemVersion.plist ProductBuildVersion)"
	
			if [[ ${#volume_version} == "6" ]]; then
				system_volume_version_short="$(defaults read /System/Library/CoreServices/SystemVersion.plist ProductVersion | cut -c-4)"
			fi
		fi

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Checked system version."${erase_style}
}


Check_Volume_Support()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Checking system support."${erase_style}

	if [[ $volume_version_short == "10."[8-9] || $volume_version_short == "10.1"[0-1] ]]; then
		echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ System support check passed."${erase_style}
	else
		echo -e $(date "+%b %d %H:%M:%S") ${text_error}"- System support check failed."${erase_style}
		echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Run this tool on a supported system."${erase_style}

		Input_On
		exit
	fi
}

Check_Volume_parrotgeek()
{
	if [ -e "$volume_path"/Library/LaunchAgents/com.parrotgeek* ]; then
		echo -e $(date "+%b %d %H:%M:%S") ${text_warning}"! Your system was patched by another patcher."${erase_style}
		echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Run this tool on a clean system."${erase_style}

		Input_On
		exit
	fi
}

Clean_Volume()
{
	Output_Off rm -R "$volume_path"/Applications/Utilities/Brightness\ Slider.app

	Output_Off rm -R "$volume_path"/Applications/Utilities/NoSleep.app
	Output_Off rm -R "$volume_path"/System/Library/Extensions/NoSleep.kext
	Output_Off rm -R "$volume_path"/System/Library/PreferencePanes/NoSleep.prefPane
}

Patch_Volume()
{
	if [[ ! $volume_version_short == "10.8" ]] || [[ $volume_version_short == "10.8" && $model_ati == *$model* ]]; then
		echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Patching boot.efi."${erase_style}

			if [[ $volume_version_short == "10.11" ]]; then
				chflags nouchg "$volume_path"/System/Library/CoreServices/boot.efi
				cp "$resources_path"/"$volume_version_short"/boot.efi "$volume_path"/System/Library/CoreServices
				chflags uchg "$volume_path"/System/Library/CoreServices/boot.efi
			else
				chflags nouchg "$volume_path"/System/Library/CoreServices/boot.efi
				cp "$resources_path"/boot.efi "$volume_path"/System/Library/CoreServices
				chflags uchg "$volume_path"/System/Library/CoreServices/boot.efi
			fi

		echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Patched boot.efi."${erase_style}
	fi


	if [[ $volume_version_short == "10.8" && ! $model_ati == *$model* ]]; then
		echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Removing 64-bit components."${erase_style}

			Output_Off rm -R "$volume_path"/System/Library/Extensions/AppleCameraInterface.kext
			Output_Off rm -R "$volume_path"/System/Library/Extensions/AppleIntelLpssDmac.kext
			Output_Off rm -R "$volume_path"/System/Library/Extensions/AppleIntelLpssGspi.kext
			Output_Off rm -R "$volume_path"/System/Library/Extensions/AppleIntelLpssSpiController.kext
			Output_Off rm -R "$volume_path"/System/Library/Extensions/AppleHSSPIHIDDriver.kext
			Output_Off rm -R "$volume_path"/System/Library/Extensions/AppleHSSPISupport.kext
			Output_Off rm -R "$volume_path"/System/Library/Extensions/AppleTopCase.kext
			Output_Off rm -R "$volume_path"/System/Library/Extensions/AppleUSBEthernetHost.kext

			Output_Off rm -R "$volume_path"/System/Library/Extensions/vecLib.kext

			Output_Off rm -R "$volume_path"/System/Library/Extensions/corecrypto.kext

			Output_Off rm -R "$volume_path"/System/Library/Extensions/IOUSBFamily.kext/Contents/PlugIns/AppleUSBXHCI.kext

			Output_Off rm -R "$volume_path"/System/Library/Extensions/ATTOExpressSASHBA3.kext

			Output_Off rm -R "$volume_path"/System/Library/UserEventPlugins/AirPortUserAgent.plugin
			Output_Off rm -R "$volume_path"/System/Library/SystemConfiguration/Apple80211Monitor.bundle
			Output_Off rm -R "$volume_path"/System/Library/SystemConfiguration/EAPOLController.bundle

		echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Removed 64-bit components."${erase_style}


		echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Patching 32-bit components."${erase_style}

			cp -R "$resources_path"/AVRCPAgent.app "$volume_path"/System/Library/CoreServices/
			cp -R "$resources_path"/BluetoothAudioAgent.app "$volume_path"/System/Library/CoreServices/
			cp -R "$resources_path"/Bluetooth\ Setup\ Assistant.app "$volume_path"/System/Library/CoreServices/
			cp -R "$resources_path"/Bluetooth.menu "$volume_path"/System/Library/CoreServices/Menu\ Extras/
			cp "$resources_path"/gmacheck "$volume_path"/System/Library/CoreServices/

			cp -R "$resources_path"/Accusys6xxxx.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/acfs.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/acfsctl.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/ALF.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/Apple16X50Serial.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleACPIPlatform.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleAHCIPort.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleAPIC.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleAVBAudio.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleBacklight.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleBacklightExpert.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleBluetoothMultitouch.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleBMC.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleEFIRuntime.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleFileSystemDriver.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleFSCompressionTypeDataless.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleFSCompressionTypeZlib.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleFWAudio.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleGraphicsControl.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleGraphicsPowerManagement.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleHIDKeyboard.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleHIDMouse.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleHPET.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleHWSensor.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleIntelCPUPowerManagement.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleIntelCPUPowerManagementClient.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleIntelFramebufferCapri.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleIntelHD3000Graphics.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleIntelHD3000GraphicsGA.plugin "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleIntelHD3000GraphicsGLDriver.bundle "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleIntelHD3000GraphicsVADriver.bundle "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleIntelHD4000Graphics.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleIntelHD4000GraphicsGA.plugin "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleIntelHD4000GraphicsGLDriver.bundle "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleIntelHD4000GraphicsVADriver.bundle "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleIntelHDGraphics.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleIntelHDGraphicsFB.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleIntelHDGraphicsGA.plugin "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleIntelHDGraphicsGLDriver.bundle "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleIntelHDGraphicsVADriver.bundle "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleIntelIVBVA.bundle "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleIntelSNBGraphicsFB.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleIntelSNBVA.bundle "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleIRController.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/Apple_iSight.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleKeyStore.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleKeyswitch.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleLPC.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleLSIFusionMPT.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleMatch.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleMCCSControl.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleMCEDriver.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleMCP89RootPortPM.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleMIDIFWDriver.plugin "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleMIDIIACDriver.plugin "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleMIDIRTPDriver.plugin "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleMIDIUSBDriver.plugin "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleMikeyHIDDriver.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleMultitouchDriver.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/ApplePlatformEnabler.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleRAID.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleRAIDCard.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleRTC.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleSDXC.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleSEP.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleSmartBatteryManager.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleSMBIOS.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleSMBusController.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleSMBusPCI.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleSMC.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleSMCLMU.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleSRP.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleStorageDrivers.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleThunderboltDPAdapters.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleThunderboltEDMService.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleThunderboltNHI.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleThunderboltPCIAdapters.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleThunderboltUTDM.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleTyMCEDriver.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleUpstreamUserClient.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleUSBAudio.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleUSBDisplays.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleUSBEthernetHost_32.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleUSBMultitouch.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleUSBTopCase.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleVADriver.bundle "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleWWANAutoEject.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleXsanFilter.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/ArcMSR.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/ATI2400Controller.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/ATI2600Controller.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/ATI3800Controller.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/ATI4600Controller.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/ATI4800Controller.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/ATI5000Controller.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/ATI6000Controller.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/ATIRadeonX2000.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/ATIRadeonX2000GA.plugin "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/ATIRadeonX2000GLDriver.bundle "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/ATIRadeonX2000VADriver.bundle "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/ATIRadeonX3000.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/ATIRadeonX3000GA.plugin "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/ATIRadeonX3000GLDriver.bundle "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/ATIRadeonX3000VADriver.bundle "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/ATTOCelerityFC.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/ATTOCelerityFC8.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/ATTOExpressPCI4.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/ATTOExpressSASHBA.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/ATTOExpressSASHBA2.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/ATTOExpressSASRAID.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/ATTOExpressSASRAID2.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AudioAUUC.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/autofs.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/BJUSBLoad.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/BootCache.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/CalDigitHDProDrv.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/cd9660.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/cddafs.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/CellPhoneHelper.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/CoreStorage.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/Dont\ Steal\ Mac\ OS\ X.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/DSACL.ppp "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/DSAuth.ppp "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/DVFamily.bundle "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/EAP-KRB.ppp "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/EAP-RSA.ppp "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/EAP-TLS.ppp "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/EPSONUSBPrintClass.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/exfat.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/GeForce7xxx.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/GeForce7xxxGA.plugin "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/GeForce7xxxVADriver.bundle "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/GeForceGLDriver.bundle "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/HighPointIOP.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/HighPointRR.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/HighPointRR644.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/hp_fax_io.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/hp_Inkjet1_io_enabler.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IO80211Family.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOAccelerator2D.plugin "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOAcceleratorFamily.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOACPIFamily.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOAHCIFamily.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOATAFamily.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOAVBFamily.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOBDStorageFamily.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOBluetoothFamily.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOBluetoothHIDDriver.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOCDStorageFamily.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IODVDStorageFamily.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOFireWireAVC.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOFireWireFamily.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOFireWireIP.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOFireWireSBP2.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOFireWireSerialBusProtocolTransport.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOGraphicsFamily.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOHDIXController.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOHIDFamily.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IONDRVSupport.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IONetworkingFamily.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOPCIFamily.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOPlatformPluginFamily.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOSCSIArchitectureModelFamily.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOSCSIParallelFamily.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOSerialFamily.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOSMBusFamily.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOStorageFamily.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOStreamFamily.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOSurface.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOThunderboltFamily.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOTimeSyncFamily.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOUSBAttachedSCSI.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOUSBFamily.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOUSBMassStorageClass.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOUserEthernet.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOVideoFamily.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/iPodDriver.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/JMicronATA.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/L2TP.ppp "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/mcxalr.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/msdosfs.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/ntfs.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/NVDAGF100Hal.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/NVDAGK100Hal.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/NVDANV40HalG7xxx.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/NVDAResmanG7xxx.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/NVSMU.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/OSvKernDSPLib.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/PPP.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/PPPoE.ppp "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/PPPSerial.ppp "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/PPTP.ppp "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/PromiseSTEX.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/Quarantine.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/Radius.ppp "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/SM56KUSBAudioFamily.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/SMARTLib.plugin "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/smbfs.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/SMCMotionSensor.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/SoftRAID.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/System.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/TMSafetyNet.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/triggers.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/udf.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/webcontentfilter.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/webdav_fs.kext "$volume_path"/System/Library/Extensions/

			cp -R "$resources_path"/afpfs.kext "$volume_path"/System/Library/Filesystems/AppleShare/
			cp -R "$resources_path"/asp_tcp.kext "$volume_path"/System/Library/Filesystems/AppleShare/
			cp -R "$resources_path"/hfs.fs "$volume_path"/System/Library/Filesystems/
			cp -R "$resources_path"/afp.bundle "$volume_path"/System/Library/Filesystems/NetFSPlugins/

			cp -R "$resources_path"/AppleShareClientCore.framework "$volume_path"/System/Library/Frameworks/
			cp -R "$resources_path"/CoreWiFi.framework "$volume_path"/System/Library/Frameworks/
			cp -R "$resources_path"/CoreWLAN.framework "$volume_path"/System/Library/Frameworks/
			cp -R "$resources_path"/IOBluetooth.framework "$volume_path"/System/Library/Frameworks/
			cp -R "$resources_path"/NetFS.framework "$volume_path"/System/Library/Frameworks/

			cp "$resources_path"/com.apple.Dock.plist "$volume_path"/System/Library/LaunchAgents/

			cp -R "$resources_path"/Bluetooth.prefPane "$volume_path"/System/Library/PreferencePanes/
			cp -R "$resources_path"/Mouse.prefPane "$volume_path"/System/Library/PreferencePanes/

			cp -R "$resources_path"/Apple80211.framework "$volume_path"/System/Library/PrivateFrameworks/
			cp -R "$resources_path"/AppleProfileFamily.framework "$volume_path"/System/Library/PrivateFrameworks/
			cp -R "$resources_path"/CoreWLANKit.framework "$volume_path"/System/Library/PrivateFrameworks/
			cp -R "$resources_path"/EAP8021X.framework "$volume_path"/System/Library/PrivateFrameworks/

			cp -R "$resources_path"/Apple80211Monitor.bundle "$volume_path"/System/Library/SystemConfiguration/
			cp -R "$resources_path"/EAPOLController.bundle "$volume_path"/System/Library/SystemConfiguration/

			cp -R "$resources_path"/SPBluetoothReporter.spreporter "$volume_path"/System/Library/SystemProfiler/
			cp -R "$resources_path"/SPDisplaysReporter.spreporter "$volume_path"/System/Library/SystemProfiler/

			cp -R "$resources_path"/AirPortUserAgent.plugin "$volume_path"/System/Library/UserEventPlugins/
			cp -R "$resources_path"/BluetoothUserAgent-Plugin.plugin "$volume_path"/System/Library/UserEventPlugins/

			cp "$resources_path"/airportd "$volume_path"/usr/libexec/
			cp "$resources_path"/wifid "$volume_path"/usr/libexec/
			cp "$resources_path"/wps "$volume_path"/usr/libexec/

			cp "$resources_path"/blued "$volume_path"/usr/sbin/
			cp "$resources_path"/bnepd "$volume_path"/usr/sbin/
			cp "$resources_path"/mDNSResponder "$volume_path"/usr/sbin/

			cp "$resources_path"/mach_kernel "$volume_path"/
			chflags hidden "$volume_path"/mach_kernel

		echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Patched 32-bit components."${erase_style}
	fi


	if [[ $volume_version_short == "10.11" ]]; then
		echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Patching input drivers."${erase_style}
			
			rm -R "$volume_path"/System/Library/Extensions/IOUSBHostFamily.kext
			cp -R "$resources_path"/AppleHIDMouse.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleIRController.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleTopCase.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleUSBMultitouch.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleUSBTopCase.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOBDStorageFamily.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOBluetoothFamily.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOBluetoothHIDDriver.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOSerialFamily.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOUSBFamily.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOUSBHostFamily.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/IOUSBMassStorageClass.kext "$volume_path"/System/Library/Extensions/
		
		echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Patched input drivers."${erase_style}
	fi


	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Patching graphics drivers."${erase_style}
		
		Output_Off rm -R "$volume_path"/System/Library/Extensions/AMD2400Controller.kext 
		Output_Off rm -R "$volume_path"/System/Library/Extensions/AMD2600Controller.kext 
		Output_Off rm -R "$volume_path"/System/Library/Extensions/AMD3800Controller.kext 
		Output_Off rm -R "$volume_path"/System/Library/Extensions/AMD4600Controller.kext 
		Output_Off rm -R "$volume_path"/System/Library/Extensions/AMD4800Controller.kext 
		Output_Off rm -R "$volume_path"/System/Library/Extensions/AMD5000Controller.kext 
		Output_Off rm -R "$volume_path"/System/Library/Extensions/AMD6000Controller.kext 
		Output_Off rm -R "$volume_path"/System/Library/Extensions/AMD7000Controller.kext 
		Output_Off rm -R "$volume_path"/System/Library/Extensions/AMD8000Controller.kext 
		Output_Off rm -R "$volume_path"/System/Library/Extensions/AMD9000Controller.kext 
		Output_Off rm -R "$volume_path"/System/Library/Extensions/AMDFramebuffer.kext 
		Output_Off rm -R "$volume_path"/System/Library/Extensions/AMDMTLBronzeDriver.bundle 
		Output_Off rm -R "$volume_path"/System/Library/Extensions/AMDRadeonVADriver.bundle 
		Output_Off rm -R "$volume_path"/System/Library/Extensions/AMDRadeonX3000.kext 
		Output_Off rm -R "$volume_path"/System/Library/Extensions/AMDRadeonX3000GLDriver.bundle 
		Output_Off rm -R "$volume_path"/System/Library/Extensions/AMDRadeonX4000.kext 
		Output_Off rm -R "$volume_path"/System/Library/Extensions/AMDRadeonX4000GLDriver.bundle 
		Output_Off rm -R "$volume_path"/System/Library/Extensions/AMDShared.bundle 
		Output_Off rm -R "$volume_path"/System/Library/Extensions/AMDSupport.kext
	
		cp -R "$resources_path"/ATI1300Controller.kext "$volume_path"/System/Library/Extensions/
		cp -R "$resources_path"/ATI1600Controller.kext "$volume_path"/System/Library/Extensions/
		cp -R "$resources_path"/ATI1900Controller.kext "$volume_path"/System/Library/Extensions/
		cp -R "$resources_path"/ATIFramebuffer.kext "$volume_path"/System/Library/Extensions/
		cp -R "$resources_path"/ATIRadeonX1000.kext "$volume_path"/System/Library/Extensions/
		cp -R "$resources_path"/ATIRadeonX1000GA.plugin "$volume_path"/System/Library/Extensions/
		cp -R "$resources_path"/ATIRadeonX1000GLDriver.bundle "$volume_path"/System/Library/Extensions/
		cp -R "$resources_path"/ATIRadeonX1000VADriver.bundle "$volume_path"/System/Library/Extensions/
		cp -R "$resources_path"/ATISupport.kext "$volume_path"/System/Library/Extensions/

		if [[ $volume_version_short == "10.8" ]]; then
			cp -R "$resources_path"/AppleIntelGMA950.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleIntelGMA950GA.plugin "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleIntelGMA950GLDriver.bundle "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleIntelGMA950VADriver.bundle "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleIntelGMAX3100.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleIntelGMAX3100FB.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleIntelGMAX3100GA.plugin "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleIntelGMAX3100GLDriver.bundle "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleIntelGMAX3100VADriver.bundle "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/AppleIntelIntegratedFramebuffer.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/GeForce.kext "$volume_path"/System/Library/Extensions/ions/
			cp -R "$resources_path"/GeForce7xxxGLDriver.bundle "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/GeForceGA.plugin "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/GeForceVADriver.bundle "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/NVDANV50Hal.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/NVDAResman.kext "$volume_path"/System/Library/Extensions/

			cp -R "$resources_path"/GeForce7xxx.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/GeForce7xxxGA.plugin "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/GeForce7xxxVADriver.bundle "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/GeForceGLDriver.bundle "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/NVDAGF100Hal.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/NVDAGK100Hal.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/NVDANV40HalG7xxx.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/NVDAResmanG7xxx.kext "$volume_path"/System/Library/Extensions/
			
			cp -R "$resources_path"/OpenCL.framework "$volume_path"/System/Library/Frameworks/
			cp -R "$resources_path"/OpenGL.framework "$volume_path"/System/Library/Frameworks/
		else
			cp -R "$resources_path"/10.9/AppleIntelGMA950.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/10.9/AppleIntelGMA950GA.plugin "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/10.9/AppleIntelGMA950GLDriver.bundle "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/10.9/AppleIntelGMA950VADriver.bundle "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/10.9/AppleIntelGMAX3100.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/10.9/AppleIntelGMAX3100FB.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/10.9/AppleIntelGMAX3100GA.plugin "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/10.9/AppleIntelGMAX3100GLDriver.bundle "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/10.9/AppleIntelGMAX3100VADriver.bundle "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/10.9/AppleIntelIntegratedFramebuffer.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/10.9/GeForce.kext "$volume_path"/System/Library/Extensions/ions/
			cp -R "$resources_path"/10.9/GeForce7xxxGLDriver.bundle "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/10.9/GeForceGA.plugin "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/10.9/GeForceVADriver.bundle "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/10.9/NVDANV50Hal.kext "$volume_path"/System/Library/Extensions/
			cp -R "$resources_path"/10.9/NVDAResman.kext "$volume_path"/System/Library/Extensions/
		fi

		if [[ $volume_version_short == "10.9" && $model_ati == *$model* ]]; then
			cp -R "$resources_path"/10.9/OpenCL.framework "$volume_path"/System/Library/Frameworks/
			cp -R "$resources_path"/10.9/OpenGL.framework "$volume_path"/System/Library/Frameworks/
			cp -R "$resources_path"/CoreGraphics.framework "$volume_path"/System/Library/Frameworks/
			cp -R "$resources_path"/QuartzCore.framework "$volume_path"/System/Library/Frameworks/
			cp -R "$resources_path"/Slideshows.framework "$volume_path"/System/Library/PrivateFrameworks/
		fi

		if [[ ! $volume_version_short == "10.8" && ! $model_ati == *$model* ]]; then
			cp "$resources_path"/com.apple.PowerManagement.plist "$volume_path"/Library/Preferences

			defaults write "$volume_path"/System/Library/LaunchDaemons/com.apple.WindowServer.plist Nice -20
		fi

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Patched graphics drivers."${erase_style}


	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Patching audio drivers."${erase_style}
		
		cp -R "$resources_path"/AppleHDA.kext "$volume_path"/System/Library/Extensions/
		cp -R "$resources_path"/IOAudioFamily.kext "$volume_path"/System/Library/Extensions/
	
	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Patched audio drivers."${erase_style}


	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Patching platform support check."${erase_style}
		
		Output_Off rm "$volume_path"/System/Library/CoreServices/PlatformSupport.plist
	
	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Patched platform support check."${erase_style}


	if [[ $volume_version_short == "10.11" ]]; then
		echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Patching kernel."${erase_style}
			
			cp "$resources_path"/kernel "$volume_path"/System/Library/Kernels
		
		echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Patched kernel."${erase_style}
	fi


	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Patching kernel flags."${erase_style}

		if [[ $model == "iMac6,1" ]]; then
			sed -i '' 's|<string></string>|<string>nv_disable=1</string>|' "$volume_path"/Library/Preferences/SystemConfiguration/com.apple.Boot.plist
		fi
	
	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Patched kernel flags."${erase_style}


	if [[ $volume_version_short == "10.11" ]]; then
		echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Patching System Integrity Protection."${erase_style}

			cp -R "$resources_path"/SIPManager.kext "$volume_path"/System/Library/Extensions

		echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Patched System Integrity Protection."${erase_style}
	fi


	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Patching kernel cache."${erase_style}
		
		Output_Off rm "$volume_path"/System/Library/Caches/com.apple.kext.caches/Startup/kernelcache
		Output_Off rm "$volume_path"/System/Library/PrelinkedKernels/prelinkedkernel
		Output_Off kextcache -f -u "$volume_path"
	
	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Patched kernel cache."${erase_style}
}

Repair()
{
	chown -R 0:0 "$@"
	chmod -R 755 "$@"
}

Repair_Permissions()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Repairing permissions."${erase_style}
		
		if [[ $volume_version_short == "10.8" ]]; then
			Repair "$volume_path"/System/Library/CoreServices/BluetoothAudioAgent.app
			Repair "$volume_path"/System/Library/CoreServices/Bluetooth\ Setup\ Assistant.app
			Repair "$volume_path"/System/Library/CoreServices/Menu\ Extras/Bluetooth.menu
			Repair "$volume_path"/System/Library/CoreServices/gmacheck

			Repair "$volume_path"/System/Library/Extensions/Accusys6xxxx.kext
			Repair "$volume_path"/System/Library/Extensions/acfs.kext
			Repair "$volume_path"/System/Library/Extensions/acfsctl.kext
			Repair "$volume_path"/System/Library/Extensions/ALF.kext
			Repair "$volume_path"/System/Library/Extensions/Apple16X50Serial.kext
			Repair "$volume_path"/System/Library/Extensions/AppleAHCIPort.kext
			Repair "$volume_path"/System/Library/Extensions/AppleAPIC.kext
			Repair "$volume_path"/System/Library/Extensions/AppleAVBAudio.kext
			Repair "$volume_path"/System/Library/Extensions/AppleBacklight.kext
			Repair "$volume_path"/System/Library/Extensions/AppleBacklightExpert.kext
			Repair "$volume_path"/System/Library/Extensions/AppleBluetoothMultitouch.kext
			Repair "$volume_path"/System/Library/Extensions/AppleBMC.kext
			Repair "$volume_path"/System/Library/Extensions/AppleEFIRuntime.kext
			Repair "$volume_path"/System/Library/Extensions/AppleFileSystemDriver.kext
			Repair "$volume_path"/System/Library/Extensions/AppleFSCompressionTypeDataless.kext
			Repair "$volume_path"/System/Library/Extensions/AppleFSCompressionTypeZlib.kext
			Repair "$volume_path"/System/Library/Extensions/AppleFWAudio.kext
			Repair "$volume_path"/System/Library/Extensions/AppleGraphicsControl.kext
			Repair "$volume_path"/System/Library/Extensions/AppleGraphicsPowerManagement.kext
			Repair "$volume_path"/System/Library/Extensions/AppleHDA.kext
			Repair "$volume_path"/System/Library/Extensions/AppleHIDKeyboard.kext
			Repair "$volume_path"/System/Library/Extensions/AppleHIDMouse.kext
			Repair "$volume_path"/System/Library/Extensions/AppleHPET.kext
			Repair "$volume_path"/System/Library/Extensions/AppleHWSensor.kext
			Repair "$volume_path"/System/Library/Extensions/AppleIntelFramebufferCapri.kext
			Repair "$volume_path"/System/Library/Extensions/AppleIntelHD3000Graphics.kext
			Repair "$volume_path"/System/Library/Extensions/AppleIntelHD3000GraphicsGA.plugin
			Repair "$volume_path"/System/Library/Extensions/AppleIntelHD3000GraphicsGLDriver.bundle
			Repair "$volume_path"/System/Library/Extensions/AppleIntelHD3000GraphicsVADriver.bundle
			Repair "$volume_path"/System/Library/Extensions/AppleIntelHD4000Graphics.kext
			Repair "$volume_path"/System/Library/Extensions/AppleIntelHD4000GraphicsGA.plugin
			Repair "$volume_path"/System/Library/Extensions/AppleIntelHD4000GraphicsGLDriver.bundle
			Repair "$volume_path"/System/Library/Extensions/AppleIntelHD4000GraphicsVADriver.bundle
			Repair "$volume_path"/System/Library/Extensions/AppleIntelHDGraphics.kext
			Repair "$volume_path"/System/Library/Extensions/AppleIntelHDGraphicsFB.kext
			Repair "$volume_path"/System/Library/Extensions/AppleIntelHDGraphicsGA.plugin
			Repair "$volume_path"/System/Library/Extensions/AppleIntelHDGraphicsGLDriver.bundle
			Repair "$volume_path"/System/Library/Extensions/AppleIntelHDGraphicsVADriver.bundle
			Repair "$volume_path"/System/Library/Extensions/AppleIntelIVBVA.bundle
			Repair "$volume_path"/System/Library/Extensions/AppleIntelSNBGraphicsFB.kext
			Repair "$volume_path"/System/Library/Extensions/AppleIntelSNBVA.bundle
			Repair "$volume_path"/System/Library/Extensions/AppleIRController.kext
			Repair "$volume_path"/System/Library/Extensions/Apple_iSight.kext
			Repair "$volume_path"/System/Library/Extensions/AppleKeyStore.kext
			Repair "$volume_path"/System/Library/Extensions/AppleKeyswitch.kext
			Repair "$volume_path"/System/Library/Extensions/AppleLPC.kext
			Repair "$volume_path"/System/Library/Extensions/AppleLSIFusionMPT.kext
			Repair "$volume_path"/System/Library/Extensions/AppleMatch.kext
			Repair "$volume_path"/System/Library/Extensions/AppleMCCSControl.kext
			Repair "$volume_path"/System/Library/Extensions/AppleMCEDriver.kext
			Repair "$volume_path"/System/Library/Extensions/AppleMIDIFWDriver.plugin
			Repair "$volume_path"/System/Library/Extensions/AppleMIDIIACDriver.plugin
			Repair "$volume_path"/System/Library/Extensions/AppleMIDIRTPDriver.plugin
			Repair "$volume_path"/System/Library/Extensions/AppleMIDIUSBDriver.plugin
			Repair "$volume_path"/System/Library/Extensions/AppleMikeyHIDDriver.kext
			Repair "$volume_path"/System/Library/Extensions/AppleMultitouchDriver.kext
			Repair "$volume_path"/System/Library/Extensions/ApplePlatformEnabler.kext
			Repair "$volume_path"/System/Library/Extensions/AppleRAID.kext
			Repair "$volume_path"/System/Library/Extensions/AppleRAIDCard.kext
			Repair "$volume_path"/System/Library/Extensions/AppleRTC.kext
			Repair "$volume_path"/System/Library/Extensions/AppleSDXC.kext
			Repair "$volume_path"/System/Library/Extensions/AppleSEP.kext
			Repair "$volume_path"/System/Library/Extensions/AppleSmartBatteryManager.kext
			Repair "$volume_path"/System/Library/Extensions/AppleSMBIOS.kext
			Repair "$volume_path"/System/Library/Extensions/AppleSMBusController.kext
			Repair "$volume_path"/System/Library/Extensions/AppleSMBusPCI.kext
			Repair "$volume_path"/System/Library/Extensions/AppleSMC.kext
			Repair "$volume_path"/System/Library/Extensions/AppleSMCLMU.kext
			Repair "$volume_path"/System/Library/Extensions/AppleSRP.kext
			Repair "$volume_path"/System/Library/Extensions/AppleStorageDrivers.kext
			Repair "$volume_path"/System/Library/Extensions/AppleThunderboltDPAdapters.kext
			Repair "$volume_path"/System/Library/Extensions/AppleThunderboltEDMService.kext
			Repair "$volume_path"/System/Library/Extensions/AppleThunderboltNHI.kext
			Repair "$volume_path"/System/Library/Extensions/AppleThunderboltPCIAdapters.kext
			Repair "$volume_path"/System/Library/Extensions/AppleThunderboltUTDM.kext
			Repair "$volume_path"/System/Library/Extensions/AppleTyMCEDriver.kext
			Repair "$volume_path"/System/Library/Extensions/AppleUpstreamUserClient.kext
			Repair "$volume_path"/System/Library/Extensions/AppleUSBAudio.kext
			Repair "$volume_path"/System/Library/Extensions/AppleUSBDisplays.kext
			Repair "$volume_path"/System/Library/Extensions/AppleUSBEthernetHost_32.kext
			Repair "$volume_path"/System/Library/Extensions/AppleUSBMultitouch.kext
			Repair "$volume_path"/System/Library/Extensions/AppleUSBTopCase.kext
			Repair "$volume_path"/System/Library/Extensions/AppleVADriver.bundle
			Repair "$volume_path"/System/Library/Extensions/AppleWWANAutoEject.kext
			Repair "$volume_path"/System/Library/Extensions/AppleXsanFilter.kext
			Repair "$volume_path"/System/Library/Extensions/ArcMSR.kext
			Repair "$volume_path"/System/Library/Extensions/ATI2400Controller.kext
			Repair "$volume_path"/System/Library/Extensions/ATI2600Controller.kext
			Repair "$volume_path"/System/Library/Extensions/ATI3800Controller.kext
			Repair "$volume_path"/System/Library/Extensions/ATI4600Controller.kext
			Repair "$volume_path"/System/Library/Extensions/ATI4800Controller.kext
			Repair "$volume_path"/System/Library/Extensions/ATI5000Controller.kext
			Repair "$volume_path"/System/Library/Extensions/ATI6000Controller.kext
			Repair "$volume_path"/System/Library/Extensions/ATIRadeonX2000.kext
			Repair "$volume_path"/System/Library/Extensions/ATIRadeonX2000GA.plugin
			Repair "$volume_path"/System/Library/Extensions/ATIRadeonX2000GLDriver.bundle
			Repair "$volume_path"/System/Library/Extensions/ATIRadeonX2000VADriver.bundle
			Repair "$volume_path"/System/Library/Extensions/ATIRadeonX3000.kext
			Repair "$volume_path"/System/Library/Extensions/ATIRadeonX3000GA.plugin
			Repair "$volume_path"/System/Library/Extensions/ATIRadeonX3000GLDriver.bundle
			Repair "$volume_path"/System/Library/Extensions/ATIRadeonX3000VADriver.bundle
			Repair "$volume_path"/System/Library/Extensions/ATTOCelerityFC.kext
			Repair "$volume_path"/System/Library/Extensions/ATTOCelerityFC8.kext
			Repair "$volume_path"/System/Library/Extensions/ATTOExpressPCI4.kext
			Repair "$volume_path"/System/Library/Extensions/ATTOExpressSASHBA.kext
			Repair "$volume_path"/System/Library/Extensions/ATTOExpressSASHBA2.kext
			Repair "$volume_path"/System/Library/Extensions/ATTOExpressSASRAID.kext
			Repair "$volume_path"/System/Library/Extensions/ATTOExpressSASRAID2.kext
			Repair "$volume_path"/System/Library/Extensions/AudioAUUC.kext
			Repair "$volume_path"/System/Library/Extensions/autofs.kext
			Repair "$volume_path"/System/Library/Extensions/BJUSBLoad.kext
			Repair "$volume_path"/System/Library/Extensions/BootCache.kext
			Repair "$volume_path"/System/Library/Extensions/CalDigitHDProDrv.kext
			Repair "$volume_path"/System/Library/Extensions/cd9660.kext
			Repair "$volume_path"/System/Library/Extensions/cddafs.kext
			Repair "$volume_path"/System/Library/Extensions/CellPhoneHelper.kext
			Repair "$volume_path"/System/Library/Extensions/CoreStorage.kext
			Repair "$volume_path"/System/Library/Extensions/Dont\ Steal\ Mac\ OS\ X.kext
			Repair "$volume_path"/System/Library/Extensions/DSACL.ppp
			Repair "$volume_path"/System/Library/Extensions/DSAuth.ppp
			Repair "$volume_path"/System/Library/Extensions/DVFamily.bundle
			Repair "$volume_path"/System/Library/Extensions/EAP-KRB.ppp
			Repair "$volume_path"/System/Library/Extensions/EAP-RSA.ppp
			Repair "$volume_path"/System/Library/Extensions/EAP-TLS.ppp
			Repair "$volume_path"/System/Library/Extensions/EPSONUSBPrintClass.kext
			Repair "$volume_path"/System/Library/Extensions/exfat.kext
			Repair "$volume_path"/System/Library/Extensions/GeForce7xxx.kext
			Repair "$volume_path"/System/Library/Extensions/GeForce7xxxGA.plugin
			Repair "$volume_path"/System/Library/Extensions/GeForce7xxxVADriver.bundle
			Repair "$volume_path"/System/Library/Extensions/GeForceGLDriver.bundle
			Repair "$volume_path"/System/Library/Extensions/HighPointIOP.kext
			Repair "$volume_path"/System/Library/Extensions/HighPointRR.kext
			Repair "$volume_path"/System/Library/Extensions/HighPointRR644.kext
			Repair "$volume_path"/System/Library/Extensions/hp_fax_io.kext
			Repair "$volume_path"/System/Library/Extensions/hp_Inkjet1_io_enabler.kext
			Repair "$volume_path"/System/Library/Extensions/IO80211Family.kext
			Repair "$volume_path"/System/Library/Extensions/IOAccelerator2D.plugin
			Repair "$volume_path"/System/Library/Extensions/IOAcceleratorFamily.kext
			Repair "$volume_path"/System/Library/Extensions/IOAHCIFamily.kext
			Repair "$volume_path"/System/Library/Extensions/IOATAFamily.kext
			Repair "$volume_path"/System/Library/Extensions/IOAudioFamily.kext
			Repair "$volume_path"/System/Library/Extensions/IOAVBFamily.kext
			Repair "$volume_path"/System/Library/Extensions/IOBDStorageFamily.kext
			Repair "$volume_path"/System/Library/Extensions/IOBluetoothFamily.kext
			Repair "$volume_path"/System/Library/Extensions/IOBluetoothHIDDriver.kext
			Repair "$volume_path"/System/Library/Extensions/IOCDStorageFamily.kext
			Repair "$volume_path"/System/Library/Extensions/IODVDStorageFamily.kext
			Repair "$volume_path"/System/Library/Extensions/IOFireWireAVC.kext
			Repair "$volume_path"/System/Library/Extensions/IOFireWireFamily.kext
			Repair "$volume_path"/System/Library/Extensions/IOFireWireIP.kext
			Repair "$volume_path"/System/Library/Extensions/IOFireWireSBP2.kext
			Repair "$volume_path"/System/Library/Extensions/IOFireWireSerialBusProtocolTransport.kext
			Repair "$volume_path"/System/Library/Extensions/IOGraphicsFamily.kext
			Repair "$volume_path"/System/Library/Extensions/IOHDIXController.kext
			Repair "$volume_path"/System/Library/Extensions/IOHIDFamily.kext
			Repair "$volume_path"/System/Library/Extensions/IONDRVSupport.kext
			Repair "$volume_path"/System/Library/Extensions/IONetworkingFamily.kext
			Repair "$volume_path"/System/Library/Extensions/IOPCIFamily.kext
			Repair "$volume_path"/System/Library/Extensions/IOPlatformPluginFamily.kext
			Repair "$volume_path"/System/Library/Extensions/IOSCSIArchitectureModelFamily.kext
			Repair "$volume_path"/System/Library/Extensions/IOSCSIParallelFamily.kext
			Repair "$volume_path"/System/Library/Extensions/IOSerialFamily.kext
			Repair "$volume_path"/System/Library/Extensions/IOSMBusFamily.kext
			Repair "$volume_path"/System/Library/Extensions/IOStorageFamily.kext
			Repair "$volume_path"/System/Library/Extensions/IOStreamFamily.kext
			Repair "$volume_path"/System/Library/Extensions/IOSurface.kext
			Repair "$volume_path"/System/Library/Extensions/IOThunderboltFamily.kext
			Repair "$volume_path"/System/Library/Extensions/IOTimeSyncFamily.kext
			Repair "$volume_path"/System/Library/Extensions/IOUSBAttachedSCSI.kext
			Repair "$volume_path"/System/Library/Extensions/IOUSBFamily.kext
			Repair "$volume_path"/System/Library/Extensions/IOUSBMassStorageClass.kext
			Repair "$volume_path"/System/Library/Extensions/IOUserEthernet.kext
			Repair "$volume_path"/System/Library/Extensions/IOVideoFamily.kext
			Repair "$volume_path"/System/Library/Extensions/iPodDriver.kext
			Repair "$volume_path"/System/Library/Extensions/JMicronATA.kext
			Repair "$volume_path"/System/Library/Extensions/L2TP.ppp
			Repair "$volume_path"/System/Library/Extensions/mcxalr.kext
			Repair "$volume_path"/System/Library/Extensions/msdosfs.kext
			Repair "$volume_path"/System/Library/Extensions/ntfs.kext
			Repair "$volume_path"/System/Library/Extensions/NVDAGF100Hal.kext
			Repair "$volume_path"/System/Library/Extensions/NVDAGK100Hal.kext
			Repair "$volume_path"/System/Library/Extensions/NVDANV40HalG7xxx.kext
			Repair "$volume_path"/System/Library/Extensions/NVDAResmanG7xxx.kext
			Repair "$volume_path"/System/Library/Extensions/NVSMU.kext
			Repair "$volume_path"/System/Library/Extensions/OSvKernDSPLib.kext
			Repair "$volume_path"/System/Library/Extensions/PPP.kext
			Repair "$volume_path"/System/Library/Extensions/PPPoE.ppp
			Repair "$volume_path"/System/Library/Extensions/PPPSerial.ppp
			Repair "$volume_path"/System/Library/Extensions/PPTP.ppp
			Repair "$volume_path"/System/Library/Extensions/PromiseSTEX.kext
			Repair "$volume_path"/System/Library/Extensions/Quarantine.kext
			Repair "$volume_path"/System/Library/Extensions/Radius.ppp
			Repair "$volume_path"/System/Library/Extensions/SM56KUSBAudioFamily.kext
			Repair "$volume_path"/System/Library/Extensions/SMARTLib.plugin
			Repair "$volume_path"/System/Library/Extensions/smbfs.kext
			Repair "$volume_path"/System/Library/Extensions/SMCMotionSensor.kext
			Repair "$volume_path"/System/Library/Extensions/SoftRAID.kext
			Repair "$volume_path"/System/Library/Extensions/System.kext
			Repair "$volume_path"/System/Library/Extensions/TMSafetyNet.kext
			Repair "$volume_path"/System/Library/Extensions/triggers.kext
			Repair "$volume_path"/System/Library/Extensions/udf.kext
			Repair "$volume_path"/System/Library/Extensions/webcontentfilter.kext
			Repair "$volume_path"/System/Library/Extensions/webdav_fs.kext

			Repair "$volume_path"/System/Library/Filesystems/AppleShare/afpfs.kext
			Repair "$volume_path"/System/Library/Filesystems/AppleShare/asp_tcp.kext
			Repair "$volume_path"/System/Library/Filesystems/hfs.fs
			Repair "$volume_path"/System/Library/Filesystems/NetFSPlugins/afp.bundle

			Repair "$volume_path"/System/Library/Frameworks/AppleShareClientCore.framework
			Repair "$volume_path"/System/Library/Frameworks/CoreWiFi.framework
			Repair "$volume_path"/System/Library/Frameworks/CoreWLAN.framework
			Repair "$volume_path"/System/Library/Frameworks/IOBluetooth.framework
			Repair "$volume_path"/System/Library/Frameworks/NetFS.framework

			Repair "$volume_path"/System/Library/LaunchAgents/com.apple.Dock.plist

			Repair "$volume_path"/System/Library/PreferencePanes/Bluetooth.prefPane
			Repair "$volume_path"/System/Library/PreferencePanes/Mouse.prefPane

			Repair "$volume_path"/System/Library/PrivateFrameworks/Apple80211.framework
			Repair "$volume_path"/System/Library/PrivateFrameworks/AppleProfileFamily.framework
			Repair "$volume_path"/System/Library/PrivateFrameworks/CoreWLANKit.framework
			Repair "$volume_path"/System/Library/PrivateFrameworks/EAP8021X.framework

			Repair "$volume_path"/System/Library/SystemConfiguration//Apple80211Monitor.bundle
			Repair "$volume_path"/System/Library/SystemConfiguration//EAPOLController.bundle

			Repair "$volume_path"/System/Library/SystemProfiler/SPBluetoothReporter.spreporter
			Repair "$volume_path"/System/Library/SystemProfiler/SPDisplaysReporter.spreporter

			Repair "$volume_path"/System/Library/UserEventPlugins/AirPortUserAgent.plugin
			Repair "$volume_path"/System/Library/UserEventPlugins/BluetoothUserAgent-Plugin.plugin

			Repair "$volume_path"/usr/libexec/airportd
			Repair "$volume_path"/usr/libexec/wifid
			Repair "$volume_path"/usr/libexec/wps

			Repair "$volume_path"/usr/sbin/blued
			Repair "$volume_path"/usr/sbin/bnepd
			Repair "$volume_path"/usr/sbin/mDNSResponder

			Repair "$volume_path"/mach_kernel
		fi

		if [[ $volume_version_short == "10.11" ]]; then
			Repair "$volume_path"/System/Library/Extensions/AppleHIDMouse.kext
			Repair "$volume_path"/System/Library/Extensions/AppleIRController.kext
			Repair "$volume_path"/System/Library/Extensions/AppleTopCase.kext
			Repair "$volume_path"/System/Library/Extensions/AppleUSBMultitouch.kext
			Repair "$volume_path"/System/Library/Extensions/AppleUSBTopCase.kext
			Repair "$volume_path"/System/Library/Extensions/IOBDStorageFamily.kext
			Repair "$volume_path"/System/Library/Extensions/IOBluetoothFamily.kext
			Repair "$volume_path"/System/Library/Extensions/IOBluetoothHIDDriver.kext
			Repair "$volume_path"/System/Library/Extensions/IOSerialFamily.kext
			Repair "$volume_path"/System/Library/Extensions/IOUSBFamily.kext
			Repair "$volume_path"/System/Library/Extensions/IOUSBHostFamily.kext
			Repair "$volume_path"/System/Library/Extensions/IOUSBMassStorageClass.kext
		fi
	
		Repair "$volume_path"/System/Library/Extensions/AppleIntelGMA950.kext
		Repair "$volume_path"/System/Library/Extensions/AppleIntelGMA950GA.plugin
		Repair "$volume_path"/System/Library/Extensions/AppleIntelGMA950GLDriver.bundle
		Repair "$volume_path"/System/Library/Extensions/AppleIntelGMA950VADriver.bundle
		Repair "$volume_path"/System/Library/Extensions/AppleIntelGMAX3100.kext
		Repair "$volume_path"/System/Library/Extensions/AppleIntelGMAX3100FB.kext
		Repair "$volume_path"/System/Library/Extensions/AppleIntelGMAX3100GA.plugin
		Repair "$volume_path"/System/Library/Extensions/AppleIntelGMAX3100GLDriver.bundle
		Repair "$volume_path"/System/Library/Extensions/AppleIntelGMAX3100VADriver.bundle
		Repair "$volume_path"/System/Library/Extensions/AppleIntelIntegratedFramebuffer.kext
		Repair "$volume_path"/System/Library/Extensions/ATI1300Controller.kext
		Repair "$volume_path"/System/Library/Extensions/ATI1600Controller.kext
		Repair "$volume_path"/System/Library/Extensions/ATI1900Controller.kext
		Repair "$volume_path"/System/Library/Extensions/ATIFramebuffer.kext
		Repair "$volume_path"/System/Library/Extensions/ATIRadeonX1000.kext
		Repair "$volume_path"/System/Library/Extensions/ATIRadeonX1000GA.plugin
		Repair "$volume_path"/System/Library/Extensions/ATIRadeonX1000GLDriver.bundle
		Repair "$volume_path"/System/Library/Extensions/ATIRadeonX1000VADriver.bundle
		Repair "$volume_path"/System/Library/Extensions/ATISupport.kext
		Repair "$volume_path"/System/Library/Extensions/GeForce.kext
		Repair "$volume_path"/System/Library/Extensions/GeForce7xxxGLDriver.bundle
		Repair "$volume_path"/System/Library/Extensions/GeForceGA.plugin
		Repair "$volume_path"/System/Library/Extensions/GeForceVADriver.bundle
		Repair "$volume_path"/System/Library/Extensions/NVDANV50Hal.kext
		Repair "$volume_path"/System/Library/Extensions/NVDAResman.kext

		Repair "$volume_path"/System/Library/Extensions/GeForce7xxx.kext 
		Repair "$volume_path"/System/Library/Extensions/GeForce7xxxGA.plugin 
		Repair "$volume_path"/System/Library/Extensions/GeForce7xxxVADriver.bundle 
		Repair "$volume_path"/System/Library/Extensions/GeForceGLDriver.bundle 
		Repair "$volume_path"/System/Library/Extensions/NVDAGF100Hal.kext 
		Repair "$volume_path"/System/Library/Extensions/NVDAGK100Hal.kext 
		Repair "$volume_path"/System/Library/Extensions/NVDANV40HalG7xxx.kext 
		Repair "$volume_path"/System/Library/Extensions/NVDAResmanG7xxx.kext

		if [[ $volume_version_short == "10.8" ]] || [[ $volume_version_short == "10.9" && $model_ati == *$model* ]]; then
			Repair "$volume_path"/System/Library/Frameworks/OpenCL.framework
			Repair "$volume_path"/System/Library/Frameworks/OpenGL.framework
		fi

		if [[ $volume_version_short == "10.9" && $model_ati == *$model* ]]; then
			Repair "$volume_path"/System/Library/Frameworks/CoreGraphics.framework
			Repair "$volume_path"/System/Library/Frameworks/QuartzCore.framework
			Repair "$volume_path"/System/Library/PrivateFrameworks/Slideshows.framework
		fi 

		Repair "$volume_path"/System/Library/Extensions/AppleHDA.kext
		Repair "$volume_path"/System/Library/Extensions/IOAudioFamily.kext

		if [[ $volume_version_short == "10.11" ]]; then
			Repair "$volume_path"/System/Library/Extensions/SIPManager.kext
		fi
	
	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Repaired permissions."${erase_style}
}

Patch_Volume_Helpers()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Patching Recovery partition."${erase_style}

		if [[ $volume_version_short == "10."[8-9] && $system_volume_version == $volume_version ]]; then
			recovery_identifier="$(dm ensureRecoveryPartition "$volume_path" /BaseSystem.dmg 0 0 /BaseSystem.chunklist|grep "RecoveryPartitionBSD"|sed 's/.*\=\ //'|sed 's/.$//')"
		else
			recovery_identifier="$(diskutil info "$volume_name"|grep "Recovery Disk"|sed 's/.*\ //')"
		fi

		if [[ ! "$(diskutil info "${recovery_identifier}"|grep "Volume Name"|sed 's/.*\  //')" == "Recovery HD" ]]; then
			echo -e $(date "+%b %d %H:%M:%S") ${text_warning}"! Error patching Recovery partition."${erase_style}
		else
			Output_Off diskutil mount "$recovery_identifier"
	
			if [[ $volume_version_short == "10.11" ]]; then
				chflags nouchg /Volumes/Recovery\ HD/com.apple.recovery.boot/boot.efi
				cp "$resources_path"/"$volume_version_short"/boot.efi /Volumes/Recovery\ HD/com.apple.recovery.boot
				chflags uchg "$volume_path"/System/Library/CoreServices/boot.efi
			else
				chflags nouchg /Volumes/Recovery\ HD/com.apple.recovery.boot/boot.efi
				cp "$resources_path"/boot.efi /Volumes/Recovery\ HD/com.apple.recovery.boot
				chflags uchg /Volumes/Recovery\ HD/com.apple.recovery.boot/boot.efi
			fi
	
			if [[ $volume_version_short == "10.11" ]]; then
				chflags nouchg /Volumes/Recovery\ HD/com.apple.recovery.boot/prelinkedkernel
				rm /Volumes/Recovery\ HD/com.apple.recovery.boot/prelinkedkernel
				cp "$volume_path"/System/Library/PrelinkedKernels/prelinkedkernel /Volumes/Recovery\ HD/com.apple.recovery.boot
				chflags uchg /Volumes/Recovery\ HD/com.apple.recovery.boot/prelinkedkernel
			fi
		
			Output_Off rm /Volumes/Recovery\ HD/com.apple.recovery.boot/PlatformSupport.plist
			Output_Off sed -i '' 's|dmg</string>|dmg -no_compat_check</string>|' /Volumes/Recovery\ HD/com.apple.recovery.boot/com.apple.boot.plist
		
			Output_Off diskutil unmount /Volumes/Recovery\ HD
	
		echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Patched Recovery partition."${erase_style}
	fi
}

End()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Thank you for using OS X Patcher."${erase_style}
	
	Input_On
	exit
}

Input_Off
Escape_Variables
Parameter_Variables
Path_Variables
Check_Environment
Check_Root
Check_Resources
Input_Model
Input_Volume
Check_Volume_Version
Check_Volume_Support
Check_Volume_parrotgeek
Clean_Volume
Patch_Volume
Repair_Permissions
Patch_Volume_Helpers
End
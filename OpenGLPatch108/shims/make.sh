#!/bin/bash -e 
GL_LIBS=/System/Library/Frameworks/OpenGL.framework/Versions/A/Libraries
MY_GL_LIBS=../OpenGL.framework/Versions/A/Libraries

cp -r fw_orig/OpenGL.framework/* ../OpenGL.framework/

gcc -fPIC -O3 -Wall -Wextra -Werror -arch x86_64 -arch i386 -dynamiclib  -current_version 1 -compatibility_version 1 -mmacosx-version-min=10.8 -flat_namespace -Wl,-reexport_library,data/libGFXShare2forlinking.dylib -o ${MY_GL_LIBS}/libGFXShared.dylib libGFXSharedShim.c
install_name_tool -id ${GL_LIBS}/libGFXShared.dylib ${MY_GL_LIBS}/libGFXShared.dylib
install_name_tool -change libGFXShare2forlinking.dylib ${GL_LIBS}/libGFXShare2.dylib ${MY_GL_LIBS}/libGFXShared.dylib

find ../Open*L.framework -name .DS_Store -type f -delete
